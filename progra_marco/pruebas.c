#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//estructura base del arbol
typedef struct nodo{
    char *alias;
    char *url;
    struct nodo *padre;
    struct nodo *izquierdo;
    struct nodo *derecho;
    struct nodo_lista *lista;
}nodo;

typedef struct nodo_lista{
	char *alias;
    char *url;
	struct nodo_lista *sig;
}nodo_lista;

//inicializa la lista vacia
nodo_lista *iniciar(nodo_lista *lista){
    lista = NULL;
    return lista;
}

//agrega nodos a lista
nodo_lista *agregar(nodo_lista *lista,char *url,char *alias){
    nodo_lista *nuevo_nodo, *tmp;
    
    nuevo_nodo = (nodo_lista *)malloc(sizeof(nodo_lista));
    nuevo_nodo->alias =alias;
    nuevo_nodo->url =url;
    nuevo_nodo->sig = NULL;
    
    if(lista == NULL){
        lista = nuevo_nodo;
    }else{
        tmp = lista;
        while(tmp->sig != NULL){
            tmp = tmp->sig;
        }
        tmp->sig = nuevo_nodo;
    }
    return lista;
}

struct nodo *crea_nodo(struct nodo *padre, char *url, char *alias){
    nodo *nuevo_nodo;
    nuevo_nodo = (nodo *)malloc(sizeof(nodo));
    nuevo_nodo->padre = padre;
    nuevo_nodo->alias = alias;
    nuevo_nodo->url = url;
    nuevo_nodo->izquierdo = NULL;
    nuevo_nodo->derecho = NULL;
    return nuevo_nodo;
}

void agrega_nodo(struct nodo *arbol, char *url, char *alias){
    nodo *tmp, *pivote;
    int izq = 0;
    //verifica que el arbol no esté vacio
    if (arbol != NULL){
        tmp = arbol;
        while(tmp != NULL){
            pivote = tmp;
           if(strcmp(url, tmp->url)>0){
                //inserta los mayores
                tmp = tmp->derecho;
                izq = 0;
            }else{
                //inserta los menores
                tmp = tmp->izquierdo;
                izq = 1;
            }
        }
        tmp = crea_nodo(pivote,url,alias);
        //esto es para insertarlos como hijos de hijos, se indica con la variable izq
        if(izq == 1){
            printf("Insertado %s como hijo izquierdo de %s\n",url,pivote->url);
            pivote->izquierdo = tmp;
        }else{
            printf("Insertado %s como hijo derecho de %s\n",url,pivote->url);
            pivote->derecho = tmp;
        }

    }else{
        printf("Árbol no iniciado");
    }
}

void imprime_nodo(nodo *nodito){
    if(nodito != NULL){
        printf("URL: %s\n",nodito->url);
        printf("Alias: %s\n",nodito->alias);
    }else{
            printf("Nodo vacio\n");
        }
    }

void preOrden(nodo *raiz){
    if(raiz != NULL){
        imprime_nodo(raiz);
        preOrden(raiz->izquierdo);
        preOrden(raiz->derecho);
    }
}

void buscar_nodo_url(nodo *raiz,char *url){
    if(raiz != NULL){
        if(strcmp(raiz->url,url)==0){
            printf("URL %s encontrado\n",url);
            printf("URL %s está denominado con el alias %s\n",raiz->url,raiz->alias);
        }else if(strcmp(url, raiz->url)>0){
            printf("URL %s será buscado en rama derecha de URL %s\n",url,raiz->url);
            raiz = raiz->derecho;
            buscar_nodo_url(raiz,url);
        }else{
            printf("URL %s será buscado en rama izquierda de URL %s\n",url,raiz->url);
            raiz = raiz->izquierdo;
            buscar_nodo_url(raiz,url);
        }
    }else{
        printf("URL no corresponde a ningun elemento en el arbol, no se encuentra en él\n");
    }
}

void buscar_nodo_alias(nodo *arbol,nodo_lista *lista,char *alias){
    if(lista!=NULL){
        if(strcmp(lista->alias,alias)==0){
            printf("Alias %s corresponde al URL %s\n", alias,lista->url);
            printf("Buscando %s en el arbol...\n", lista->url);
            buscar_nodo_url(arbol,lista->url);
        }else{
            lista = lista->sig;
            buscar_nodo_alias(arbol,lista,alias);
        }
    }else{
        printf("Alias %s no se encuentra presente en el arbol actualmente\n",alias);
    }
}


void imprimir_lista(nodo_lista *lista){
    if(lista != NULL){
    while(lista != NULL){
        printf("ALIAS: %s, URL: %s\n", lista->alias,lista->url);
        lista = lista->sig;
    }
    }else{
        puts("Lista vacía");
    }
}

// Función para conseguir una entrada del usuario
char* get_user_input(size_t max_size){
    
    char *buffer;
    size_t characters;

    buffer = (char *)malloc(max_size * sizeof(char));
    if(buffer == NULL){
        perror("ERROR No fue posible reservar memoria para el buffer");
        exit(1);
    }
    characters = getline(&buffer,&max_size,stdin);
    buffer[strlen(buffer)-1]= 0;
    return buffer;
}

int main(){
    nodo *arbol = NULL;
    nodo_lista *lista;
    int opcion;
    char *url_ingresada;
    char *alias_ingresado;
    size_t max_size;
    lista = iniciar(lista);

	do{
		printf("---------------------------------------------------------------------\n");
	    printf("Seleccione una opcion por favor:\n");
        printf("0. Salir del programa\n");
        printf("1. Ingresar una URL y un Alias\n");
        printf("2. Imprimir el arbol actual\n");
        printf("3. Imprimir la lista actual\n");
		printf("---------------------------------------------------------------------\n");
		printf("Opcion deseada: \n");
        scanf("%d",&opcion);

		switch(opcion){
			case 0: 
				break;
			case 1: 
                max_size = 50; // Este tamaño se puede editar
                
                get_user_input(max_size); // Inicializa el get_user_input, por alguna razón se ocupa hacer esto

                // Pide el URL
                url_ingresada = (char *) malloc(max_size);
                printf("URL: ");
                url_ingresada = get_user_input(max_size);
                
                // Pide el alias
                alias_ingresado = (char *) malloc(max_size);
                printf("Alias: ");
                alias_ingresado = get_user_input(max_size);


                if(lista != NULL){

                    agrega_nodo(arbol,url_ingresada,alias_ingresado);
                    lista = agregar(lista,url_ingresada,alias_ingresado);
                }else{

                    arbol = crea_nodo(NULL,url_ingresada,alias_ingresado);
                    lista = agregar(lista,url_ingresada,alias_ingresado);
            }
            break;
			case 2:
				preOrden(arbol);
				break;
			case 3:
				imprimir_lista(lista);
				break;
		}
	}while(opcion != 0);
    return 0;
}







        /*
        if(strcmp(arbol->url, arbol->padre->izquierdo->url) == 0){
            arbol->padre->izquierdo = nuevo_nodo;
        
        // Cuando tiene un hijo derecho hace esto
        }else if(strcmp(arbol->url, arbol->padre->derecho->url) == 0){
            arbol->padre->derecho = nuevo_nodo;
        
        }
        */
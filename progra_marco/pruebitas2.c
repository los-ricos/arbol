#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define True 1
#define False 0

//estructura base del arbol
typedef struct nodo{
    char alias[60];
    char url[120];
    struct nodo *padre;
    struct nodo *izquierdo;
    struct nodo *derecho;
}nodo;

typedef struct nodo_lista{
	char alias[60];
    char url[120];
	struct nodo_lista *sig;
}nodo_lista;

typedef struct nodos{
    struct nodo *arbol;
    struct nodo_lista *lista;
}nodos;

//inicializa la lista vacia
nodo_lista *iniciar_lista(nodo_lista *lista){
    lista = NULL;
    return lista;
}

nodo *iniciar_arbol(nodo *arbol){
    arbol = NULL;
    return arbol;
}

//agrega nodos a lista
nodo_lista *agregar(nodo_lista *lista,char url[120],char alias[60]){
    nodo_lista *nuevo_nodo, *tmp;
    
    nuevo_nodo = (nodo_lista *)malloc(sizeof(nodo_lista));
    strcpy(nuevo_nodo->alias,alias);
    strcpy(nuevo_nodo->url,url);
    nuevo_nodo->sig = NULL;
    
    if(lista == NULL){
        lista = nuevo_nodo;
    }else{
        tmp = lista;
        while(tmp->sig != NULL){
            tmp = tmp->sig;
        }
        tmp->sig = nuevo_nodo;
    }
    return lista;
}

struct nodo *crea_nodo(struct nodo *padre, char url[120], char alias[60]){
    nodo *nuevo_nodo;
    nuevo_nodo = (nodo *)malloc(sizeof(nodo));
    nuevo_nodo->padre = padre;
    strcpy(nuevo_nodo->alias,alias);
    strcpy(nuevo_nodo->url,url);
    nuevo_nodo->izquierdo = NULL;
    nuevo_nodo->derecho = NULL;
    return nuevo_nodo;
}

nodos *agrega_estructura(struct nodo *arbol, struct nodo_lista *lista){
    nodos *nuevo_nodo;
    nuevo_nodo = (nodos *)malloc(sizeof(nodos));
    nuevo_nodo->arbol = arbol;
    nuevo_nodo->lista = lista;
    return nuevo_nodo;
}

void *agrega_nodo(struct nodo *arbol, char url[120], char alias[60]){
    nodo *ind, *tmp;
    int izq = 0;
    //verifica que el arbol no esté vacio
    if (arbol != NULL){
        ind = arbol;
        while(ind != NULL){
            tmp = ind;
           if(strcmp(url, ind->url)<0){
                //inserta los mayores
                ind = ind->derecho;
                izq = 0;
            }else{
                //inserta los menores
                ind = ind->izquierdo;
                izq = 1;
            }
        }
        ind = crea_nodo(tmp,url,alias);
        //esto es para insertarlos como hijos de hijos, se indica con la variable izq
        if(izq == 1){
            printf("Insertado %s como hijo izquierdo de %s\n",url,tmp->url);
            tmp->izquierdo = ind;
        }else{
            printf("Insertado %s como hijo derecho de %s\n",url,tmp->url);
            tmp->derecho = ind;
        }

    }else{
        printf("Árbol no iniciado");
    }
}

void imprime_nodo(nodo *nodito){
    if(nodito != NULL){
        printf("URL: %s\n",nodito->url);
        printf("Alias: %s\n",nodito->alias);
    }
}

void in_orden(nodo *raiz){
    if(raiz != NULL){
        in_orden(raiz->izquierdo);
        imprime_nodo(raiz);
        in_orden(raiz->derecho);
    }
}

int buscar_nodo_url(nodo *raiz,char url[120]){

    if(raiz != NULL){
        if(strcmp(raiz->url,url)==0){
            printf("URL %s encontrado\n",url);
            printf("URL %s está denominado con el alias %s\n",raiz->url,raiz->alias);
            return True;

        }else if(strcmp(url, raiz->url)>0){
            printf("URL %s será buscado en rama derecha de URL %s\n",url,raiz->url);
            raiz = raiz->derecho;
            buscar_nodo_url(raiz,url);
        }else{
            printf("URL %s será buscado en rama izquierda de URL %s\n",url,raiz->url);
            raiz = raiz->izquierdo;
            buscar_nodo_url(raiz,url);
        }
    }else{
        printf("URL no corresponde a ningun elemento en el arbol, no se encuentra en él\n");
        return False;
    }
}

void buscar_nodo_alias(nodo *arbol,nodo_lista *lista,char alias[60]){
    if(lista!=NULL){
        if(strcmp(lista->alias,alias)==0){
            printf("Alias %s corresponde al URL %s\n", alias,lista->url);
            printf("Buscando %s en el arbol...\n", lista->url);
            buscar_nodo_url(arbol,lista->url);
        }else{
            lista = lista->sig;
            buscar_nodo_alias(arbol,lista,alias);
        }
    }else{
        printf("Alias %s no se encuentra presente en el arbol actualmente\n",alias);
    }
}

// Esta función busca el nodo a eliminar, "dato" debe ser URL
// Hacer que eliminar retorne el arbol para que sirva cuando se quiera eliminar la raíz
void eliminar(nodo *arbol, char url_eliminar[120]){

    if(arbol != NULL){

        if(strcmp(arbol->url, url_eliminar) < 0){
            // Si el valor es menor, busca por la izquierda
            eliminar(arbol->izquierdo, url_eliminar);

        }else if(strcmp(arbol->url, url_eliminar) > 0){
            // Si el dato es mayor, busca por la derecha
            
            eliminar(arbol->derecho, url_eliminar);

        }else{
        
            eliminarNodo(arbol);
        
        }

    }else{
        printf("\nEl url no está ingresado, intente de nuevo.\n");
    }
    

}

// Función para buscar el hijo más izquierdo
nodo *minimo(nodo *arbol, int bandera){

    if(bandera == 1){

        if(arbol == NULL){
            return NULL;

        }if(arbol->izquierdo){
            return minimo(arbol->izquierdo, 1); // busca la parte más izquierda

        }else{
            return arbol;
        }

    }else{

        if(arbol == NULL){
            return NULL;

        }if(arbol->izquierdo){
            return minimo(arbol->izquierdo, 0); // busca la parte más izquierda

        }else if(arbol->derecho){
            return minimo(arbol->derecho, 0);

        }else{
            return arbol; // retorna el mismo nodo si no tiene hijo izquierdo

        }

    }
 

}

// Función que reemplaza un nodo por otro
void reemplazar(nodo *arbol, nodo *nuevo_nodo){

    if(arbol->padre != NULL){ // Si el arbol sí tiene padre, se le debe asignar al padre su  nuevo hijo y a ese hijo el nuevo padre
        
        // Pregunta si tiene un nodo izquierdo, si es así va a entrar a hacer la comparación y si es igual elimina
        if(arbol->padre->izquierdo != NULL){
            if(strcmp(arbol->url, arbol->padre->izquierdo->url) == 0){
                arbol->padre->izquierdo = nuevo_nodo;

            }

        }
        // Si el anterior no es el caso, pregunta si tiene derecho, si es así entra a comparar y si es igual elimina
        if(arbol->padre->derecho != NULL){
            if(strcmp(arbol->url, arbol->padre->derecho->url) == 0){
                arbol->padre->derecho = nuevo_nodo;

            }

        }

    }

}

// Función para eliminar el nodo eliminado
void eliminarNodo(nodo *nodo_eliminar){

    if(nodo_eliminar->padre != NULL){

        // Caso en que el nodo tiene los 2 hijos
        if(nodo_eliminar->izquierdo && nodo_eliminar->derecho){ // CASO PROBADO
            nodo *menor = minimo(nodo_eliminar->derecho, 1);
            strcpy(nodo_eliminar->alias, menor->alias);
            strcpy(nodo_eliminar->url, menor->url);
            eliminarNodo(menor);
            printf("CASO DE AMBOS HIJOS\n");

        }// Caso para eliminar un solo hijo izquierdo
        else if(nodo_eliminar->izquierdo){ // CASO PROBADO
            printf("CASO DE HIJO IZQUIERDO\n");
            reemplazar(nodo_eliminar, nodo_eliminar->izquierdo);
            free(nodo_eliminar); // Probando esta línea, no sé si sirve bien, si no, tengo que crear otra función

        }// Caso para eliminar un solo hijo derecho
        else if(nodo_eliminar->derecho){ // CASO PROBADO
            printf("CASO DE HIJO DERECHO\n");
            reemplazar(nodo_eliminar, nodo_eliminar->derecho);
            free(nodo_eliminar);
            
        }else{ // Caso en que el nodo no tiene hijos, osea es hoja
            reemplazar(nodo_eliminar, NULL); // CASO PROBADO
            printf("CASO HOJA\n");
            free(nodo_eliminar);
        }

    }else{ // Este else es para cuando se quiera eliminar la raíz
        printf("Caso de raíz\n");

        if(nodo_eliminar->izquierdo && nodo_eliminar->derecho){
            printf("CASO DE AMBOS HIJOS\n");
            nodo *menor = minimo(nodo_eliminar->izquierdo, 0);
            strcpy(nodo_eliminar->alias, menor->alias);
            strcpy(nodo_eliminar->url, menor->url);
            eliminarNodo(menor);

        }else if(nodo_eliminar->izquierdo){
            printf("CASO DE HIJO IZQUIERDO\n");
            nodo *menor = minimo(nodo_eliminar->izquierdo, 0);
            strcpy(nodo_eliminar->alias, menor->alias);
            strcpy(nodo_eliminar->url, menor->url);
            eliminarNodo(menor);

        }else if(nodo_eliminar->derecho){
            printf("CASO DE HIJO DERECHO\n");
            nodo *menor = minimo(nodo_eliminar->derecho, 0);
            strcpy(nodo_eliminar->alias, menor->alias);
            strcpy(nodo_eliminar->url, menor->url);
            eliminarNodo(menor);

        }else{
            reemplazar(nodo_eliminar, NULL);
            printf("CASO HOJA\n");
            free(nodo_eliminar);
            nodo_eliminar = NULL;

        }

    }


}

void imprimir_lista(nodo_lista *lista){
    if(lista != NULL){
    while(lista != NULL){
        printf("ALIAS: %s, URL: %s\n", lista->alias,lista->url);
        lista = lista->sig;
    }
    }else{
        puts("Lista vacía");
    }
}


//Descarga los urls de la pagina
void descargar_urls(){

    //Cierra todos los exploradores para que no haya error    
    system("killall chrome"); 
    //system("killall firefox")
    
    //Abre una pagina web con el buscador predeterminado
    system("xdg-open https://moz.com/top-500/download/?table=top500Domains");

    //Espera 5 segundo para que se descargue el archivo
    system("sleep 5");

    //Cierra el buscador
    system("killall -KILL chrome");


}

void abrir_url(char link[180]){
    char abrir[200];
    char ini[20];
    strcpy(ini,"xdg-open ");

    strcpy(abrir,strcat(ini,link));
    
    system (abrir);

}

nodos *agregar_links_descargados(nodo *raiz,nodo_lista *lista){

    nodos *padevolver;

    //Leer de un archivo
    FILE * archivo = NULL;
    
    //Busca el archivo en descargas ****
    char * nombre_archivo = "/home/estudiante/Escritorio/Proyecto/arbol/progra_randall/top500Domains.csv";
    
    //Variable que almacena lo que se lee en el archivo
    char *lectura;
    char palabra[120];
    
    

    //Abrimos el archivo en solo lectura
    archivo = fopen(nombre_archivo,"r");

    int caracter;
    int contador=0;
    char comilla[2]= "\"";
    char coma[2]= ",";
    char salto[2]="\n";
    int columna = 1;
    char http[40];
    //Si el open tira un error
    if (archivo == NULL)
    {
        printf("Error al leer archivo");
    }
        
    //Salta las primeras lines que son los títulos
    while (caracter !=salto[0])
    {
        caracter=fgetc(archivo);
    }
    
    

    
    while (1)
    {
        caracter = fgetc(archivo);
        if (feof(archivo))
            {
                break;
            }
        else if (caracter == salto[0])
        {
            columna=1;
            contador=0;

            strcpy(http,"https://");
            lectura=strcat(http,palabra);
            
            if(raiz != NULL){
                agrega_nodo(raiz,lectura,"NULL");
                lista = agregar(lista,lectura,"NULL");
            }else{
                raiz = crea_nodo(raiz,lectura,"NULL");
                lista = agregar(lista,lectura,"NULL");
            }
            memset(palabra,0,120);
        }

        else if (caracter== coma[0]){

            columna ++;

        }
        else if (columna == 2 && caracter != comilla[0])
        {   
            palabra[contador]=caracter;
            contador++;
        }


    }
    
    //Se cierra el archivo
    fclose(archivo);
    padevolver = agrega_estructura(raiz,lista);
    return padevolver;
   
}


int main(){
    nodo *arbol;
    nodo_lista *lista;
    nodos *estructura;
    int opcion;
    char url_ingresada[120];
    char alias_ingresado[60];
    char url_eliminar[120];
    char url_buscar[120];
    lista = iniciar_lista(lista);
    arbol = iniciar_arbol(arbol);

	do{
		printf("-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n");
	    printf("|Seleccione una opcion por favor:                                    |\n");
        printf("|0. Salir del programa                                               |\n");
        printf("|1. Ingresar una URL y un Alias                                      |\n");
        printf("|2. Imprimir el arbol actual                                         |\n");
        printf("|3. Imprimir la lista actual                                         |\n");
		printf("|4. Descargar los 500 links                                          |\n");
        printf("|5. Inserta los 500 links                                            |\n");
        printf("|6. Abrir un link                                                    |\n");
        printf("|7. Eliminar un nodo                                                 |\n");
        printf("-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n");
		printf("Opcion deseada: \n");
        scanf("%d",&opcion);

		switch(opcion){
			case 0: 
				break;
			case 1: 
                printf("Inserte su URL: \n");
                scanf("%s",url_ingresada);

                printf("Inserte su alias: \n");
                scanf("%s",alias_ingresado);
                
                printf("--------------------\n");
                printf("|Agregando al arbol|\n");
                printf("--------------------\n");
                printf("%s siendo agregado al arbol\n",url_ingresada);
               
                if(arbol != NULL){

                    agrega_nodo(arbol,url_ingresada,alias_ingresado);
                    lista = agregar(lista,url_ingresada,alias_ingresado);
                }else{

                    arbol = crea_nodo(arbol,url_ingresada,alias_ingresado);
                    lista = agregar(lista,url_ingresada,alias_ingresado);
            }
            break;
			case 2:
                printf("-------------------\n");
                printf("|Imprimiendo arbol|\n");
                printf("-------------------\n");
                if(arbol != NULL){
                    in_orden(arbol);
                }else{
                    printf("Arbol vacio\n");
                }
				break;
			case 3:
                printf("-------------------\n");
                printf("|Imprimiendo lista|\n");
                printf("-------------------\n");
				imprimir_lista(lista);
				break;

            case 4:
                printf("-------------------\n");
                printf("|Descargando 500 links|\n");
                printf("-------------------\n");  
                descargar_urls();          
                break; 
            case 5:
                printf("-------------------\n");
                printf("|Insertando 500 links|\n");
                printf("-------------------\n");
                estructura = agregar_links_descargados(arbol,lista);
                arbol = estructura->arbol;
                lista = estructura->lista;
                printf("%s", arbol->url);
                break;
            case 6:
                
                printf("Ingrese el URL que desea abrir\n");
                scanf("%s", url_buscar);

                if(buscar_nodo_url(arbol, url_buscar)){
                    printf("El url a abrir es %s\n", url_buscar);
                    abrir_url(url_buscar);

                }
                
                break;

            case 7:
                printf("Indique la URL del nodo a eliminar\n");
                scanf("%s", url_eliminar);

                if(arbol == NULL){
                    printf("El arbol no tiene nodos para eliminar\n");
                    

                }else if(arbol->derecho == NULL && arbol->izquierdo == NULL){
                    free(arbol);
                    arbol = iniciar_arbol(arbol);
                
                }else{
                    eliminar(arbol, url_eliminar);

                }

                break;
        }
	}while(opcion != 0);
    return 0;
}


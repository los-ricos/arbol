/*
Tecnologico de Costa Rica
Estructuras de datos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define True 1
#define False 0

/*-----------------------------------------------------------------------
	nodo_arbol
	Funcionamiento: 
        - Contiene estructura para cada nodo perteneciente al arbol binario
-----------------------------------------------------------------------*/
typedef struct nodo_arbol{
    char alias[60];
    char url[120];
    struct nodo_arbol *padre;
    struct nodo_arbol *izquierdo;
    struct nodo_arbol *derecho;
}nodo_arbol;

/*-----------------------------------------------------------------------
	nodo_lista
	Funcionamiento: 
        - Contiene estructura para cada nodo perteneciente a una lista enlazada
-----------------------------------------------------------------------*/
typedef struct nodo_lista{
	char alias[60];
    char url[120];
	struct nodo_lista *sig;
}nodo_lista;

/*-----------------------------------------------------------------------
	nodo_lista
	Funcionamiento: 
        - Contiene estructura para un nodo que guarda dos punteros,
        indican principio de arbol y principio de lista
-----------------------------------------------------------------------*/
typedef struct nodo{
    struct nodo_arbol *arbol;
    struct nodo_lista *lista;
}nodo;

/*-----------------------------------------------------------------------
	iniciar_lista
	Entradas: Un puntero de tipo nodo_lista
    Salidas: Un puntero de tipo nodo_lista
    Restricciones: debe ser un puntero de tipo nodo_lista
	Funcionamiento: 
        - Establece la lista como NULL
-----------------------------------------------------------------------*/
nodo_lista *iniciar_lista(nodo_lista *lista){
    lista = NULL;
    return lista;
}

/*-----------------------------------------------------------------------
	iniciar_arbol
	Entradas: Un puntero de tipo nodo_arbol
    Salidas: Un puntero de tipo nodo_arbol
    Restricciones: debe ser un puntero de tipo nodo_arbol
	Funcionamiento: 
        - Establece el arbol como NULL
-----------------------------------------------------------------------*/
nodo_arbol *iniciar_arbol(nodo_arbol *arbol){
    arbol = NULL;
    return arbol;
}

/*-----------------------------------------------------------------------
	agregar
	Entradas: Un puntero de tipo nodo_lista, dos char
    Salidas: Un puntero de tipo nodo_lista
    Restricciones: deben ser un puntero y dos char
	Funcionamiento: 
        - guarda espacio para un nodo de lista
        - verifica si está vacia e inserta el primero
        - de lo contrario lo agrega al final de la lista actual
-----------------------------------------------------------------------*/
nodo_lista *agregar(nodo_lista *lista,char url[120],char alias[60]){
    nodo_lista *nuevo_nodo, *tmp;
    
    nuevo_nodo = (nodo_lista *)malloc(sizeof(nodo_lista));
    strcpy(nuevo_nodo->alias,alias);
    strcpy(nuevo_nodo->url,url);
    nuevo_nodo->sig = NULL;
    
    if(lista == NULL){
        lista = nuevo_nodo;
    }else{
        tmp = lista;
        while(tmp->sig != NULL){
            tmp = tmp->sig;
        }
        tmp->sig = nuevo_nodo;
    }
    return lista;
}

/*-----------------------------------------------------------------------
	crea_nodo
	Entradas:  Un puntero de tipo nodo_arbol, dos char
    Salidas: Un puntero de tipo nodo_arbol
    Restricciones: deben ser un puntero y dos char
	Funcionamiento: 
        - guarda espacio para un nodo de arbol
        - asigna cada valor a la estructura
-----------------------------------------------------------------------*/
struct nodo_arbol *crea_nodo(struct nodo_arbol *padre, char url[120], char alias[60]){
    nodo_arbol *nuevo_nodo;
    nuevo_nodo = (nodo_arbol *)malloc(sizeof(nodo_arbol));
    nuevo_nodo->padre = padre;
    strcpy(nuevo_nodo->alias,alias);
    strcpy(nuevo_nodo->url,url);
    nuevo_nodo->izquierdo = NULL;
    nuevo_nodo->derecho = NULL;
    return nuevo_nodo;
}

/*-----------------------------------------------------------------------
	agrega_estructura
	Entradas: Un puntero de tipo nodo_arbol y otro de tipo nodo_lista
    Salidas: Un puntero de tipo nodo
    Restricciones: deben ser dos punteros
	Funcionamiento: 
        - guarda espacio para un nodo
        - asigna cada valor a la estructura
-----------------------------------------------------------------------*/
nodo *agrega_estructura(struct nodo_arbol *arbol, struct nodo_lista *lista){
    nodo *nuevo_nodo;
    nuevo_nodo = (nodo *)malloc(sizeof(nodo));
    nuevo_nodo->arbol = arbol;
    nuevo_nodo->lista = lista;
    return nuevo_nodo;
}

/*-----------------------------------------------------------------------
	agrega_nodo
	Entradas: Un puntero de tipo nodo_arbol, dos char
    Salidas: No posee
    Restricciones: deben ser un puntero y dos char
	Funcionamiento: 
        - Verifica que arbol esté iniciado
        - Guarda la raiz del arbol si está en NULL
        - Realiza comparacion de menores y mayores
        - Guarda los hijos de cada nodo
-----------------------------------------------------------------------*/
void agrega_nodo(struct nodo_arbol *arbol, char url[120], char alias[60]){
    nodo_arbol *ind, *tmp;
    int izq = 0;
    //verifica que el arbol no esté vacio
    if (arbol != NULL){
        ind = arbol;
        while(ind != NULL){
            tmp = ind;
           if(strcmp(url, ind->url)<0){
                //inserta los mayores
                ind = ind->derecho;
                izq = 0;
            }else{
                //inserta los menores
                ind = ind->izquierdo;
                izq = 1;
            }
        }
        ind = crea_nodo(tmp,url,alias);
        //esto es para insertarlos como hijos de hijos, se indica con la variable izq
        if(izq == 1){
            printf("Insertado %s como hijo izquierdo de %s\n",url,tmp->url);
            tmp->izquierdo = ind;
        }else{
            printf("Insertado %s como hijo derecho de %s\n",url,tmp->url);
            tmp->derecho = ind;
        }

    }else{
        printf("Árbol no iniciado");
    }
}

/*-----------------------------------------------------------------------
	imprime_nodo
	Entradas: Un puntero de tipo nodo_arbol
    Salidas: No posee
    Restricciones: deben ser un puntero tipo nodo_arbol
	Funcionamiento: 
        - imprime lo que contenga el nodo recibido
-----------------------------------------------------------------------*/
void imprime_nodo(nodo_arbol *nodo){
    if(nodo != NULL){
        printf("URL: %s\n",nodo->url);
        printf("Alias: %s\n",nodo->alias);
    }
}

/*-----------------------------------------------------------------------
	in_orden
	Entradas: Un puntero de tipo nodo_arbol
    Salidas: No posee
    Restricciones: deben ser un puntero tipo nodo_arbol
	Funcionamiento: 
        - recorre en inorden el arbol
        - envia a imprimir cada nodo en inorden
-----------------------------------------------------------------------*/
void in_orden(nodo_arbol *raiz){
    if(raiz != NULL){
        in_orden(raiz->izquierdo);
        imprime_nodo(raiz);
        in_orden(raiz->derecho);
    }
}

/*-----------------------------------------------------------------------
	buscar_nodo_url
	Entradas: Un puntero de tipo nodo_arbol y un char
    Salidas: un entero
    Restricciones: deben ser un puntero tipo nodo_arbol y un char
	Funcionamiento: 
        - verifica según su tamaño cada nodo
        - si es menor o mayor se devuelve hasta que sea igual
        - si es igual se imprime encontrado
        - si no se encuentra en ningún momento se imprime que no está
-----------------------------------------------------------------------*/
int buscar_nodo_url(nodo_arbol *raiz,char url[120]){

    if(raiz != NULL){
        if(strcmp(raiz->url,url)==0){
            printf("URL %s encontrado\n",url);
            printf("URL %s está denominado con el alias %s\n",raiz->url,raiz->alias);
            return True;

        }else if(strcmp(url, raiz->url)<0){
            printf("URL %s será buscado en rama derecha de URL %s\n",url,raiz->url);
            raiz = raiz->derecho;
            buscar_nodo_url(raiz,url);
        }else{
            printf("URL %s será buscado en rama izquierda de URL %s\n",url,raiz->url);
            raiz = raiz->izquierdo;
            buscar_nodo_url(raiz,url);
        }
    }else{
        printf("URL no corresponde a ningun elemento en el arbol, no se encuentra en él\n");
        return False;
    }
}

/*-----------------------------------------------------------------------
	buscar_nodo_alias
	Entradas: Un puntero de tipo nodo_arbol, otro de tipo nodo_lista y un char
    Salidas: No posee
    Restricciones: deben ser un puntero tipo nodo_arbol, otro de tipo nodo_lista y un char
	Funcionamiento: 
        - verifica según su tamaño cada nodo
        - al ser igual se busca por medio de url
        - si son diferentes recorre la lista
        - si no lo encuentra imprime un mensaje
-----------------------------------------------------------------------*/
void buscar_nodo_alias(nodo_arbol *arbol,nodo_lista *lista,char alias[60]){
    if(lista!=NULL){
        if(strcmp(lista->alias,alias)==0){
            printf("Alias %s corresponde al URL %s\n", alias,lista->url);
            printf("Buscando %s en el arbol...\n", lista->url);
            buscar_nodo_url(arbol,lista->url);
        }else{
            lista = lista->sig;
            buscar_nodo_alias(arbol,lista,alias);
        }
    }else{
        printf("Alias %s no se encuentra presente en el arbol actualmente\n",alias);
    }
}

// Esta función busca el nodo a eliminar, "dato" debe ser URL
// Hacer que eliminar retorne el arbol para que sirva cuando se quiera eliminar la raíz
void eliminar(nodo_arbol *arbol, char url_eliminar[120]){

    if(arbol != NULL){

        if(strcmp(arbol->url, url_eliminar) < 0){
            // Si el valor es menor, busca por la izquierda
            eliminar(arbol->izquierdo, url_eliminar);

        }else if(strcmp(arbol->url, url_eliminar) > 0){
            // Si el dato es mayor, busca por la derecha
            
            eliminar(arbol->derecho, url_eliminar);

        }else{
        
            eliminarNodo(arbol);
        
        }

    }
    

}

// Función para buscar el hijo más izquierdo
nodo_arbol *minimo(nodo_arbol *arbol){

    if(arbol == NULL){
        return NULL;

    }if(arbol->izquierdo){
        return minimo(arbol->izquierdo); // busca la parte más izquierda

    }else{
        return arbol; // retorna el mismo nodo si no tiene hijo izquierdo
    }

}

// Función que reemplaza un nodo por otro
void reemplazar(nodo_arbol *arbol, nodo_arbol *nuevo_nodo){

    if(arbol->padre != NULL){ // Si el arbol sí tiene padre, se le debe asignar al padre su  nuevo hijo y a ese hijo el nuevo padre
        
        // Pregunta si tiene un nodo izquierdo, si es así va a entrar a hacer la comparación y si es igual elimina
        if(arbol->padre->izquierdo != NULL){
            if(strcmp(arbol->url, arbol->padre->izquierdo->url) == 0){
                arbol->padre->izquierdo = nuevo_nodo;

            }

        }
        // Si el anterior no es el caso, pregunta si tiene derecho, si es así entra a comparar y si es igual elimina
        if(arbol->padre->derecho != NULL){
            if(strcmp(arbol->url, arbol->padre->derecho->url) == 0){
                arbol->padre->derecho = nuevo_nodo;

            }

        }

    }else{
        nodo_arbol *temp = arbol->izquierdo;
        free(arbol);

    }
    /*
    if(nuevo_nodo == NULL){
        // ME FALTA ESTE CASO BASE, CUANDO HAY QUE ELIMINAR AL NODO RAÍZ.
        // SE BORRA EL ALIAS PERO NO EL NODO COMO TAL
        nuevo_nodo->padre = arbol->padre;
    
        

        //nuevo_nodo->padre = arbol->padre;
    }
    */
}

// Función para eliminar el nodo eliminado
void eliminarNodo(nodo_arbol *nodo_eliminar){
    
    // Caso en que el nodo tiene los 2 hijos
    if(nodo_eliminar->izquierdo && nodo_eliminar->derecho){ // CASO PROBADO
        nodo_arbol *menor = minimo(nodo_eliminar->derecho);
        strcpy(nodo_eliminar->alias, menor->alias);
        strcpy(nodo_eliminar->url, menor->url);
        eliminarNodo(menor);
        printf("CASO DE AMBOS HIJOS\n");

    }// Caso para eliminar un solo hijo izquierdo
    else if(nodo_eliminar->izquierdo){ // CASO PROBADO
        printf("CASO DE HIJO IZQUIERDO\n");
        reemplazar(nodo_eliminar, nodo_eliminar->izquierdo);
        free(nodo_eliminar); // Probando esta línea, no sé si sirve bien, si no, tengo que crear otra función

    }// Caso para eliminar un solo hijo derecho
    else if(nodo_eliminar->derecho){ // CASO PROBADO
        printf("CASO DE HIJO DERECHO\n");
        reemplazar(nodo_eliminar, nodo_eliminar->derecho);
        free(nodo_eliminar);
        
    }else{ // Caso en que el nodo no tiene hijos, osea es hoja
        reemplazar(nodo_eliminar, NULL); // CASO PROBADO
        printf("CASO HOJA\n");
        free(nodo_eliminar);
    }

}

/*-----------------------------------------------------------------------
	imprimir_lista
	Entradas: Un puntero de tipo nodo_lista
    Salidas: No posee
    Restricciones: deben ser un puntero tipo nodo_lista
	Funcionamiento: 
        - verifica si la lista está vacia
        - si no lo está imprime su contenido
-----------------------------------------------------------------------*/
void imprimir_lista(nodo_lista *lista){
    if(lista != NULL){
    while(lista != NULL){
        printf("ALIAS: %s, URL: %s\n", lista->alias,lista->url);
        lista = lista->sig;
    }
    }else{
        puts("Lista vacía");
    }
}


//Descarga los urls de la pagina
void descargar_urls(){

    //Cierra todos los exploradores para que no haya error    
    system("killall chrome"); 
    //system("killall firefox")
    
    //Abre una pagina web con el buscador predeterminado
    system("xdg-open https://moz.com/top-500/download/?table=top500Domains");

    //Espera 5 segundo para que se descargue el archivo
    system("sleep 5");

    //Cierra el buscador
    system("killall -KILL chrome");


}

void abrir_url(char link[180]){
    char abrir[200];
    char ini[20];
    strcpy(ini,"xdg-open ");

    strcpy(abrir,strcat(ini,link));
    
    system (abrir);

}

nodo *agregar_links_descargados(nodo_arbol *raiz,nodo_lista *lista){

    nodo *tmp;

    //Leer de un archivo
    FILE * archivo = NULL;
    
    //Busca el archivo en descargas ****
    char * nombre_archivo = "/home/jeremy/Descargas/top500Domains.csv";
    
    //Variable que almacena lo que se lee en el archivo
    char *lectura;
    char palabra[120];
    
    

    //Abrimos el archivo en solo lectura
    archivo = fopen(nombre_archivo,"r");

    int caracter;
    int contador=0;
    char comilla[2]= "\"";
    char coma[2]= ",";
    char salto[2]="\n";
    int columna = 1;
    char http[40];
    //Si el open tira un error
    if (archivo == NULL)
    {
        printf("Error al leer archivo");
    }
        
    //Salta las primeras lines que son los títulos
    while (caracter !=salto[0])
    {
        caracter=fgetc(archivo);
    }
    
    

    
    while (1)
    {
        caracter = fgetc(archivo);
        if (feof(archivo))
            {
                break;
            }
        else if (caracter == salto[0])
        {
            columna=1;
            contador=0;

            strcpy(http,"https://");
            lectura=strcat(http,palabra);
            
            if(raiz != NULL){
                agrega_nodo(raiz,lectura,"NULL");
                lista = agregar(lista,lectura,"NULL");
            }else{
                raiz = crea_nodo(raiz,lectura,"NULL");
                lista = agregar(lista,lectura,"NULL");
            }
            memset(palabra,0,120);
        }

        else if (caracter== coma[0]){

            columna ++;

        }
        else if (columna == 2 && caracter != comilla[0])
        {   
            palabra[contador]=caracter;
            contador++;
        }


    }
    
    //Se cierra el archivo
    fclose(archivo);
    tmp = agrega_estructura(raiz,lista);
    return tmp;
   
}

/*-----------------------------------------------------------------------
	cambiar_alias_arbol
	Entradas: dos punteros, uno de tipo nodo_lista y otro tipo nodo_arbol, un char
    Salidas: No posee salidas
    Restricciones: deben ser dos punteros y un char
	Funcionamiento: 
        - Verifica nodo por nodo si el url coincide y cambia el alias de ese nodo
        - Llama a cambiar_alias_lista para modificar la lista tambien
-----------------------------------------------------------------------*/
void cambiar_alias_arbol(nodo_lista *lista,nodo_arbol *raiz,char url[120]){
    char nuevo_alias[60];

        if(strcmp(raiz->url,url)==0){
            printf("Ingrese el nuevo alias: \n");
            scanf("%s",nuevo_alias);
            strcpy(raiz->alias,nuevo_alias);
            cambiar_alias_lista(lista,nuevo_alias,url);

        }else if(strcmp(url, raiz->url)<0){
            raiz = raiz->derecho;
            cambiar_alias_arbol(lista,raiz,url);
        }else{
            raiz = raiz->izquierdo;
            cambiar_alias_arbol(lista,raiz,url);
        }
}

/*-----------------------------------------------------------------------
	cambiar_alias_arbol
	Entradas: un puntero tipo nodo_lista y dos char
    Salidas: No posee salidas
    Restricciones: deben un puntero y un char
	Funcionamiento: har
        - Verifica nodo por nodo si el url coincide y cambia el alias de ese nodo
-----------------------------------------------------------------------*/
void cambiar_alias_lista(nodo_lista *lista,char nuevo_alias[60],char url[120]){

        if(strcmp(lista->url,url)==0){
            strcpy(lista->alias,nuevo_alias);
        }else{
            lista = lista->sig;
            cambiar_alias_lista(lista,nuevo_alias,url);
        }
}

int main(){
    nodo_arbol *arbol;
    nodo_lista *lista;
    nodo *estructura;
    int opcion;
    char url_ingresada[120];
    char alias_ingresado[60];
    char url_eliminar[120];
    char url_buscar[120];
    char alias_buscar[60];
    lista = iniciar_lista(lista);
    arbol = iniciar_arbol(arbol);

	do{
		printf("-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n");
	    printf("|Seleccione una opcion por favor:                                    |\n");
        printf("|0. Salir del programa                                               |\n");
        printf("|1. Ingresar una URL y un Alias                                      |\n");
        printf("|2. Imprimir el arbol actual                                         |\n");
        printf("|3. Imprimir la lista actual                                         |\n");
		printf("|4. Descargar los 500 links                                          |\n");
        printf("|5. Inserta los 500 links                                            |\n");
        printf("|6. Abrir un link                                                    |\n");
        printf("|7. Eliminar un nodo                                                 |\n");
        printf("|8. Buscar un nodo por URL                                           |\n");
        printf("|9. Buscar un nodo por alias                                         |\n");
        printf("|10. Modificar un alias                                              |\n");
        printf("-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n");
		printf("Opcion deseada: \n");
        scanf("%d",&opcion);

		switch(opcion){
			case 0: 
				break;
			case 1: 
                printf("Inserte su URL: \n");
                scanf("%s",url_ingresada);

                printf("Inserte su alias: \n");
                scanf("%s",alias_ingresado);
                
                printf("--------------------\n");
                printf("|Agregando al arbol|\n");
                printf("--------------------\n");
                printf("%s siendo agregado al arbol\n",url_ingresada);
               
                if(arbol != NULL){

                    agrega_nodo(arbol,url_ingresada,alias_ingresado);
                    lista = agregar(lista,url_ingresada,alias_ingresado);
                }else{

                    arbol = crea_nodo(arbol,url_ingresada,alias_ingresado);
                    lista = agregar(lista,url_ingresada,alias_ingresado);
            }
            break;
			case 2:
                printf("-------------------\n");
                printf("|Imprimiendo arbol|\n");
                printf("-------------------\n");
                if(arbol != NULL){
                    in_orden(arbol);
                }else{
                    printf("Arbol vacio\n");
                }
				break;
			case 3:
                printf("-------------------\n");
                printf("|Imprimiendo lista|\n");
                printf("-------------------\n");
				imprimir_lista(lista);
				break;

            case 4:
                printf("-------------------\n");
                printf("|Descargando 500 links|\n");
                printf("-------------------\n");  
                descargar_urls();          
                break; 
            case 5:
                printf("-------------------\n");
                printf("|Insertando 500 links|\n");
                printf("-------------------\n");
                estructura = agregar_links_descargados(arbol,lista);
                arbol = estructura->arbol;
                lista = estructura->lista;
                
                break;
            case 6:
                
                printf("Ingrese el URL que desea abrir\n");
                scanf("%s", url_buscar);

                if(buscar_nodo_url(arbol, url_buscar)== True){
                    printf("El url a abrir es %s\n", url_buscar);
                    abrir_url(url_buscar);

                }
                
                break;

            case 7:
                printf("Indique la URL del nodo a eliminar\n");
                scanf("%s", url_eliminar);

                if(arbol != NULL){
                    eliminar(arbol, url_eliminar);

                }else{
                    printf("El arbol no tiene nodos para eliminar\n");
                }

                break;
            case 8:
                printf("Ingrese el URL que desea buscar\n");
                scanf("%s", url_buscar);

                if(buscar_nodo_url(arbol, url_buscar)== True){
                    printf("Busqueda finalizada exitosamente\n");
                }else{
                    printf("Url %s no encontrado\n",url_buscar);
                }
                break;
            case 9:
                printf("Ingrese el alias que desea buscar\n");
                scanf("%s", alias_buscar);
                buscar_nodo_alias(arbol,lista,alias_buscar);
                break;
            case 10:
                printf("Ingrese el url del alias que desea modificar\n");
                scanf("%s", url_buscar);
                if(buscar_nodo_url(arbol, url_buscar)== True){
                    cambiar_alias_arbol(lista, arbol, url_buscar);
                }else{
                    printf("Url %s no encontrado\n",url_buscar);
                }
                break;
        }
	}while(opcion != 0);
    return 0;
}